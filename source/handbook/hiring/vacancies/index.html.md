---
layout: markdown_page
title: "Vacancies"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

For a listing of open jobs, please see the [vacancies section on the Jobs page](/jobs#vacancies). Vacancies are maintained in Lever based on our [job descriptions](/handbook/hiring/#job-descriptions). A vacancy is a temporarily open position, a job description is a permanent description that also applies to existing people with that title. Don't use vacancy and job description interchangeably. The relevant parts of a job description are copy-pasted to Lever to open a vacancy.

## Vacancy Creation Process

If you want to hire for a position you will need to follow the steps below.
A vacancy needs to be opened in Lever before we can start interviewing.

1. Review the job description in the `/jobs` folder and update, if needed, by making a merge request. If there is no job description, follow the steps to [create a new job](/handbook/hiring/jobs/#new-job-creation), then continue. The recruiters will use the relevant elements from this job description to open the role in Lever.
  * The job description should include:
    * Responsibilities
    * Specialty (e.g. Gitaly, Backend)
    * Level
    * Location (e.g. Anywhere, EMEA, Americas)
    * Requirements
    * Hiring Process, also update the internal [hiring process repo](https://gitlab.com/gitlab-com/people-ops/hiring-processes)
1. Ensure the proper approvals are in place to hire for the job.
  * The job must be listed on the "Hiring Forecast" (found on the Google Drive).
  * If the role is not listed on the "Hiring Forecast," the hiring manager will obtain approval from the Executive team member. The Executive team member escalates to the CFO and/or the CEO.
  * Roles listed on the Hiring Forecast will have a one of the following statuses:
    - Open - vacancy is open and we are actively sourcing and hiring for the role
    - Closed - no vacancy, not actively sourcing or hiring for the role
    - Filled - vacancy has been filled by internal or external candidate
    - Draining - vacancy is closed, no longer accepting new applicants, but will review existing candidates through the hiring process and fill the role if a qualified candidate is identified from existing applicants
    - Pending Approval - vacancy has been suggested by executive team member and is pending board, CEO, or CFO approval.
1. To open a vacancy, please go to the [People Ops Employment project](https://gitlab.com/gitlab-com/peopleops/employment/issues)
and create a new issue. Next to title where it says "Choose a template", select the [vacancy](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/vacancy.md) template. Follow the directions that populate in the issue.
1. Once the issue is complete, People Ops will [open the job in Lever](/handbook/hiring/vacancies/#posting-the-role-in-lever).

To find which recruiter works on what role, search the Google Drive for "Recruiter Roles Division".

### Posting the Role in Lever

Once all steps in the vacancy issue are completed, a member of the Recruiting team will create the position in [Lever](https://hire.lever.co/jobs), using the exact same job title and job description as provided in the issue. If this step is completed out of order, people are able to apply even though the position posting may not have been approved yet.

1. In Lever, click "Create Job Posting" in top right corner.
1. For location, select a specific timezone or "Remote".
1. For department/team, select the appropriate team.
1. For work type, choose full-time or intern.
1. Under "Job Posting Status" you can choose whether this job will be published upon creation, kept internal, or a draft.
1. Description:
  * Copy and paste any opening paragraphs from the job description.
  * Beneath that, copy and paste the "About GitLab" section from another vacancy, which includes a short blurb about GitLab, what we value, and the top 10 reasons to work at GitLab.
1. Under `Lists` create a new lists titled "Responsibilities" and "Requirements"
  * Copy and paste the relevant information from responsibilities, requirements, specialty, etc. from the job description.
  * Repeat with any other sections as needed.  
  * Ensure the language and structure is gender neutral using this [gender decoder](http://gender-decoder.katmatfield.com/). Make necessary edits to the vacancy description, and create a merge request to update the job description.
* Include the hiring process in the `Closing` section, along with a link to the compensation calculator for the role if applicable.
  - For some positions, it may be possible or desirable to exclusively seek candidates outside of higher cost regions. This is a role by role decision between the Hiring Manager and People Operations. _If_ it is decided to seek candidates outside of higher cost regions, then that needs to be explicitly communicated on the position description page in order to set fair expectations for candidates and recruiters alike. Sample text to consider placing on the position description page: "Globally, xx % of people live in metro areas with a rent index greater than yy, while at GitLab, this is zz % of people in this role currently. Therefore, in line with our value of staying [frugal](/handbook/values), we are now focusing on only hiring in metro areas with a **rent index lower than yy**. Please bear this in mind when using the compensation calculator below." (obviously, fill in the missing numbers).
* Under `Custom Questions`, add the following: "Additional Questions", "Salary", and "Cover Letter". If there are additional specific questions for that role, either select the appropriate form or create a new one.
* Finally, you can also customize the default Feedback Forms that interviewers will complete during different stages of the interview. If there is no custom feedback form, choose "General Feedback".
* Once finished, click "Create Posting". If you chose "published" under "Job Posting Status", the job will become live on our jobs page.

All job openings must be posted on our careers page for at least 5 business days before we can close it or make an offer; this includes all new positions and [promotions](/handbook/people-operations/promotions-transfers/#promotions).

## Publicize the job

The manager should always ask the team for passive referrals for open positions. GitLab team members can refer candidates through our [referral program](/handbook/incentives/#referral-bonuses)

The employment team will **always** publicize the job through the following means:

1. Tweet the new job post with the help of the content marketing manager and team.
1. Request "soft” referrals by encouraging all GitLab team members to post links to the jobs site on their LinkedIn profiles.
1. [Hacker News Who's Hiring](https://news.ycombinator.com/ask): On the first of the month, include a note for GitLab in the Hacker News thread of "Who's Hiring" . Template text:
`REMOTE ONLY GitLab - We're hiring production engineers, developers, designers, and more, see https://about.gitlab.com/jobs/ We're a remote only company so everyone can participate and contribute equally. GitLab Community Edition is an open-source Ruby on Rails project with over 1000 contributors.`

**Note**: The employment team may advertise the job through the following sites and is open to posting to more, in order to widely publish a variety of vacancies:

1. [Hacker News Jobs](https://news.ycombinator.com/jobs) as a Y Combinator alumni we can post directly to the front page. The EA vault has credentials so ask an EA to post. Template text: `GitLab (YC W15) is hiring XXX and more - Remote Only`
1. [LinkedIn](https://www.linkedin.com/) (Able to post 3 jobs simultaneously, please mention to employment team if you want your role listed here)
1. [StackOverflow](http://stackoverflow.com/jobs) (Able to post 3 jobs simultaneously, please mention to employment team if you want your role listed here)
1. [TechLadies](https://www.hiretechladies.com/) (Able to post 4 roles simultaneously, please mention to employment team if you want your role listed here; at this time we are not posting engineering roles to TechLadies)
1. [PowerToFly](https://powertofly.com/) (All current vacancies are updated biweekly)
1. [RemoteBase](https://remotebase.io/) (Free; job descriptions are synced directly to our respective job description sites)
1. [WeWorkRemotely](https://weworkremotely.com) ($200 for 30 days, per position, used infrequently)
1. [RemoteOK](https://remoteok.io) ($200 for 90 days, per position, used infrequently)
1. [Indeed Prime](http://www.indeed.com/) (Primarily used for non-engineering roles)
1. [Ruby Weekly](https://rubyweekly.com) ($199 per slot per newsletter, for engineering roles)

## Sourcing for Open Positions

For difficult or hard-to-fill positions, the employment team will use available tools to source for additional candidates. Please communicate with the employment team if sourcing is needed for a strategic, specialized, or difficult to fill position. In addition, managers should also reach out to their own network for candidates and referrals. It is common for candidates to respond more frequently to those who they know are the hiring manager. One superpower of great managers is having a strong network of talent from which to source.

## Closing a vacancy

To close a vacancy:

1. The employment team will clear the pipeline of candidates in all stages of application. Consider adding tags to candidates who were interesting but were passed over in this hiring process. Adding tags makes it easier to find them in Lever later on if you are recruiting for the same or a similar position. You can also snooze a candidate if you anticipate reopening the role at a later date.
1. Ask a Lever admin (People Ops) to close the position in Lever.

If the position was posted on any job site (i.e. Stack Overflow, PowerToFly) the employment team will email the partner or remove the position from that site.
